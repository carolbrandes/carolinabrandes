
let links = document.querySelectorAll(".menu-principal__a");
let camisetas = document.querySelector("#camisetas");
let calcas = document.querySelector("#calcas");
let sapatos = document.querySelector("#sapatos");
let titulopagina = document.querySelector(".produtos__h1");
let tituloPagina2 = document.querySelector(".indicador-pagina");
let contato = document.querySelector(".contato");
let filtros_ordenar_produto = document.querySelector(".flex-filtrar-dropbox");
let numero_paginas = document.querySelector(".numero_paginas");
let produtos_filtro = document.querySelector(".flex-principal");

links.forEach(element => {

  element.addEventListener('click', function (e) {
    e.preventDefault();
    if (element.innerHTML == "Camisetas") {
        camisetas.style.display = "block";
        calcas.style.display = "none";
        sapatos.style.display = "none";
        titulopagina.innerHTML = "Camisetas";
         tituloPagina2.innerHTML = "Camisetas";
         contato.style.display = "none";
    }

    if (element.innerHTML == "Calças") {
        camisetas.style.display = "none";
        calcas.style.display = "block";
        sapatos.style.display = "none";
        titulopagina.innerHTML = "Calças";
         tituloPagina2.innerHTML = "Calças";
         contato.style.display = "none";
    }

    if (element.innerHTML == "Sapatos") {
        camisetas.style.display = "none";
        calcas.style.display = "none";
        sapatos.style.display = "block";
        titulopagina.innerHTML = "Sapatos";
         tituloPagina2.innerHTML = "Sapatos";
         contato.style.display = "none";
    }

    if (element.innerHTML == "Página Inicial") {
        camisetas.style.display = "block";
        calcas.style.display = "block";
        sapatos.style.display = "block";
        titulopagina.innerHTML = "Todos Produtos";
         tituloPagina2.innerHTML = "Todos Produtos";
         contato.style.display = "none";
    }

    if (element.innerHTML == "Contato") {
        camisetas.style.display = "none";
        calcas.style.display = "none";
        sapatos.style.display = "none";
        contato.style.display = "block";
        filtros_ordenar_produto.style.display = "none";
        produtos_filtro.style.display = "none";
        numero_paginas.style.display ="none";
        titulopagina.innerHTML = "Contato";
         tituloPagina2.innerHTML = "Contato";
    }

  })


});
